#include "panel_ui.h"
#include "match.h"
#include <string>

#include "panel_ui.moc"

#define wait_for(x) while(x) \
                QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

panel_ui::panel_ui(QWidget* parent) : QWidget(parent)
{
        setupUi(this);

        // load font
        int font_id = QFontDatabase::addApplicationFont(qApp->applicationDirPath() + "/images/Font.ttf");
        QString font_name = QFontDatabase::applicationFontFamilies(font_id).at(0);

        // set style sheets
        home_score->setStyleSheet(QString("font: 28px \"%1\"; color: rgb(255, 255, 255);").arg(font_name));
        away_score->setStyleSheet(QString("font: 28px \"%1\"; color: rgb(255, 255, 255);").arg(font_name));
        movements[0] = 0; movements[1] = 0;
        clean_up();

        // singals -> slots
        connect(this,   SIGNAL(all_ready()),
                this,   SLOT(display()));

        change(match());
}

void panel_ui::move(direction dir)
{
        if(m_me_ready || movements[0] || movements[1])
                return;
        QRect rect = geometry();
        QRect par_rect = ((QWidget*)parent())->geometry();

        QRect edges[] = {rect, rect, rect, rect};
        edges[0].moveLeft(-rect.width());
        edges[1].moveRight(2 * par_rect.width());
        edges[2].moveTop(-rect.height());
        edges[3].moveBottom(2 * par_rect.height());

        movements[0] = new QPropertyAnimation(this, "geometry");
        movements[0]->setDuration(500);
        movements[0]->setStartValue(rect);
        movements[0]->setEndValue(edges[dir]);
        movements[1] = new QPropertyAnimation(this, "geometry");
        movements[1]->setDuration(500);
        movements[1]->setStartValue(edges[((int)(dir / 2)) * 2 + (dir % 2 ? 0 : 1)]);
        movements[1]->setEndValue(rect);

        if(movements[0]) {
                connect(movements[0],   SIGNAL(finished()),
                        this,           SLOT(me_ready()));
                movements[0]->start();
        }
}

void panel_ui::me_ready()
{
        m_me_ready = true;
        if(m_me_ready && m_dl_ready)
                emit all_ready();
}

void panel_ui::dl_ready()
{
        m_dl_ready = true;
        if(m_me_ready && m_dl_ready)
                emit all_ready();
}

void panel_ui::display()
{
        match m;
        emit need_match(m);
        change(m);

        if(movements[1]) {
                connect(movements[1],   SIGNAL(finished()),
                        this,           SLOT(clean_up()));
                movements[1]->start();
        } else clean_up();
        emit finished();
}

void panel_ui::change(match m)
{
        std::cout << m.home << " " <<
        m.home_score << ":" <<
        m.away_score << " " <<
        m.away << " " <<
        m.state << "@" <<
        m.time << std::endl;

        ////////////// state:
        if(m.state == "0") {
                m.home_score = "000";
                m.away_score = "000"; }
        ////////////// home:
        m.home = qApp->applicationDirPath().toStdString() + "/images/teams-logo/" + m.home;
        home_logo->setPixmap(QPixmap(m.home.c_str()));
        ////////////// away:
        m.away = qApp->applicationDirPath().toStdString() + "/images/teams-logo/" + m.away;
        away_logo->setPixmap(QPixmap(m.away.c_str()));
        ////////////// home_score:
        for(int i = m.home_score.length(); i < 3; i++)
                m.home_score = "0" + m.home_score;
        home_score->setText(m.home_score.c_str());
        ////////////// away_score:
        for(int i = m.away_score.length(); i < 3; i++)
                m.away_score = "0" + m.away_score;
        away_score->setText(m.away_score.c_str());
}

void panel_ui::clean_up()
{
        if(movements[0]) delete movements[0];
        if(movements[1]) delete movements[1];
        memset(movements, 0, sizeof(movements));

        m_me_ready = false;
        m_dl_ready = false;
}
