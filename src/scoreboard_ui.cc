#include "scoreboard_ui.h"
#include "scoreboard_ui.moc"
#include "control_ui.h"
#include "icon.png.h"

scoreboard_ui::scoreboard_ui(QWidget* parent) : QWidget(parent)
{
        // member new/stuff
        m_container = new QWidget(this);
        m_panel = new panel_ui(m_container);
        m_bkg = new QPixmap(qApp->applicationDirPath() + "/images/ui/bkg_image.png");
        m_date = QDate::currentDate();
        m_downler = new download_thread();
        m_timer = new QTimer();
        m_control = new control_ui(this);
        m_downler->local = qApp->applicationDirPath().toStdString() + "/tmp/match_list.xml";
        m_downler->log = qApp->applicationDirPath().toStdString() + "/tmp/get_info.log";
        m_downler->timeout = 10000;
        m_team = 0;
        m_state = 0;

        //singal -> slot
        connect(m_panel,   SIGNAL(need_match(match&)), this, SLOT(gen_match(match&)));
        connect(m_downler, SIGNAL(finished()),         this, SLOT(downloaded()));
        connect(m_timer,   SIGNAL(timeout()),          this, SLOT(static_update()));
        connect(m_control, SIGNAL(prev_day()),         this, SLOT(prev_day()));
        connect(m_control, SIGNAL(prev_team()),        this, SLOT(prev_team()));
        connect(m_control, SIGNAL(next_day()),         this, SLOT(next_day()));
        connect(m_control, SIGNAL(next_team()),        this, SLOT(next_team()));
        connect(m_control, SIGNAL(click_close()),      qApp, SLOT(quit()));

        // widgets settings
        QPixmap icon; icon.loadFromData(icon_png, icon_png_size);
        this->setWindowIcon(icon);
        this->setFixedSize(m_bkg->size());
        this->setAttribute(Qt::WA_TranslucentBackground);
        this->setWindowFlags(Qt::FramelessWindowHint |
                             Qt::Tool |
                             Qt::WindowStaysOnTopHint);
        m_control->setGeometry(QRect(QPoint(0, 0), m_bkg->size()));
        m_control->show();

        m_container->setGeometry(QRect(QPoint(65, 20), m_bkg->size()- QSize(100, 55)));
        m_panel->setGeometry(QRect(QPoint(0, 0), m_container->geometry().size()));
        m_panel->show();

        m_timer->start(60000);
        move(panel_ui::right);
}

////////////////////////////////////////////////////////////
// function of window moving

void scoreboard_ui::mousePressEvent(QMouseEvent * event)
{
        if(event->buttons() | Qt::LeftButton) {
                m_down[0] = geometry().topLeft();
                m_down[1] = event->globalPos();
        }
}

void scoreboard_ui::mouseMoveEvent(QMouseEvent * event)
{
        if(event->buttons() | Qt::LeftButton) {
                QPoint moved = event->globalPos() - m_down[1];
                QPoint top_left = m_down[0] + moved;
                setGeometry(QRect(top_left, geometry().size()));
        }
}

void scoreboard_ui::paintEvent(QPaintEvent* event)
{
        QPainter(this).drawPixmap(0, 0,
                m_bkg->width(), m_bkg->height(), *m_bkg);
}

///////////////////////////////////////////////////////////
// common

void scoreboard_ui::move(panel_ui::direction_t dir)
{
        if(dir == panel_ui::left || dir == panel_ui::right) {
                m_downler->remote = "http://shihira.duapp.com/match_list.py?date=" +
                        m_date.toString("yyyy-MM-dd").toStdString();
                m_downler->start();
        } else m_panel->dl_ready();

        m_panel->move(dir);
}

void scoreboard_ui::downloaded()
{
        m_ml.load_file(qApp->applicationDirPath().toStdString() + "/tmp/match_list.xml");
        if(m_panel->state())
                m_panel->dl_ready();
        else {
                match m;
                gen_match(m);
                m_panel->change(m);
        }
}

///////////////////////////////////////////////////////////
// change slots

void scoreboard_ui::static_update()
{
        m_downler->remote = "http://shihira.duapp.com/match_list.py?date=" +
                m_date.toString("yyyy-MM-dd").toStdString();
        m_downler->start();
}

void scoreboard_ui::next_team()
{
        if(m_panel->state()) return;
        if(m_ml.size() == 1) return;
        m_team += m_team < m_ml.size() - 1 ? 1 : -m_team;
        move(panel_ui::up);
}

void scoreboard_ui::next_day()
{
        if(m_panel->state()) return;
        m_team = 0;
        m_date = m_date.addDays(1);
        move(panel_ui::left);
}

void scoreboard_ui::prev_day()
{
        if(m_panel->state()) return;
        m_team = 0;
        m_date = m_date.addDays(-1);
        move(panel_ui::right);
}

void scoreboard_ui::prev_team()
{
        if(m_panel->state()) return;
        if(m_ml.size() == 1) return;
        m_team -= m_team > 0 ? 1 : -(m_ml.size() - 1);
        move(panel_ui::down);
}

void scoreboard_ui::gen_match(match& m) { m = m_ml.index(m_team); }

