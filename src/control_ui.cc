#include "control_ui.h"
#include "control_ui.moc"

control_ui::control_ui(QWidget* parent) : QWidget(parent)
{
        setupUi(this);

        mb_close = new image_button(this);

        mb_close->set_pixmap(QPixmap(qApp->applicationDirPath() + "/images/ui/close.png"));
        mb_next_day->set_pixmap(QPixmap(qApp->applicationDirPath() + "/images/ui/next_day.png"));
        mb_prev_day->set_pixmap(QPixmap(qApp->applicationDirPath() + "/images/ui/prev_day.png"));
        mb_next_team->set_pixmap(QPixmap(qApp->applicationDirPath() + "/images/ui/next_team.png"));
        mb_prev_team->set_pixmap(QPixmap(qApp->applicationDirPath() + "/images/ui/prev_team.png"));

        connect(mb_next_day,  SIGNAL(clicked()), this, SIGNAL(next_day()));
        connect(mb_next_team, SIGNAL(clicked()), this, SIGNAL(next_team()));
        connect(mb_prev_day,  SIGNAL(clicked()), this, SIGNAL(prev_day()));
        connect(mb_prev_team, SIGNAL(clicked()), this, SIGNAL(prev_team()));
        connect(mb_close,     SIGNAL(clicked()), this, SIGNAL(click_close()));
}

void control_ui::resizeEvent(QResizeEvent *event)
{
        mb_close->move(width() - mb_close->width() - 18, 0);
}

void control_ui::leaveEvent(QEvent* event)
{
        mb_next_day->set_hide(1);
        mb_prev_day->set_hide(1);
        mb_next_team->set_hide(1);
        mb_prev_team->set_hide(1);
        mb_close->set_hide(1);
}

void control_ui::enterEvent(QEvent* event)
{
        mb_next_day->set_hide(0);
        mb_prev_day->set_hide(0);
        mb_next_team->set_hide(0);
        mb_prev_team->set_hide(0);
        mb_close->set_hide(0);
}
