#include "scoreboard_ui.h"
#include <tinyxml.h>

#include <QApplication>

int main(int argc, char* argv[])
{
        QApplication app(argc, argv);

        QDir().mkpath(app.applicationDirPath() + "/tmp");

        scoreboard_ui* window = new scoreboard_ui();
        window->show();

        app.exec();
}

