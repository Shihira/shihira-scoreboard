#ifndef  scoreboard_ui_INC
#define  scoreboard_ui_INC

#include <QtGui>

#include "panel_ui.h"
#include "control_ui.h"
#include "match.h"
#include "sinc/netfile.h"

class download_thread : public QThread, public netfile {
protected:
        virtual void run() {
                std::cout << "GET:" << remote << '\n';
                download();
        }
};

class scoreboard_ui : public QWidget {
        Q_OBJECT
        
public:
        scoreboard_ui(QWidget* parent = 0);

protected:
        match_list m_ml;
        panel_ui* m_panel;
        control_ui* m_control;
        QPixmap* m_bkg;
        QTimer* m_timer;
        QWidget* m_container;
        download_thread* m_downler;

        QPoint m_down[2]; // 0-window 1-global
        QDate m_date;
        int m_team;
        int m_state; //1 - moving; -1 - still;

protected:
        virtual void mousePressEvent(QMouseEvent * event);
        virtual void mouseMoveEvent(QMouseEvent * event);
        virtual void paintEvent(QPaintEvent* event);

protected slots:
        void move(panel_ui::direction_t dir);
        void downloaded();

public slots:
        void next_day();
        void prev_day();
        void next_team();
        void prev_team();
        void static_update();

        inline void gen_match(match& m);
};

#endif   /* ----- #ifndef scoreboard_ui_INC  ----- */
