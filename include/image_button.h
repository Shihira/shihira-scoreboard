#ifndef  IMAGE_BUTTON_INC
#define  IMAGE_BUTTON_INC

#include <QtGui>

class image_button : public QToolButton {
        const float change_alpha;
        const int timer_ms;

protected:
        QPixmap m_pixmap;

        int m_state; // 0normal 1hover 2press 3hiden
        float m_cur_alpha;
        float m_alpha_array[4];

        void paintEvent(QPaintEvent* event) {
                QPainter painter(this);
                painter.setOpacity(m_cur_alpha);
                painter.drawPixmap(m_pixmap.rect(), m_pixmap);
                painter.end();
        }

        void timerEvent(QTimerEvent* event) {
                float plus_minus = m_alpha_array[m_state] - m_cur_alpha;
                if(fabs(plus_minus) < change_alpha)
                        m_cur_alpha = m_alpha_array[m_state];
                else if(fabs(plus_minus) > change_alpha) {
                        plus_minus = plus_minus > 0 ? 1 : -1;
                        m_cur_alpha += plus_minus * change_alpha;
                } else return;
                update();
        }

        void enterEvent(QEvent* event) { m_state = 1; }
        void leaveEvent(QEvent* event) { m_state = 0; }

        void mousePressEvent(QMouseEvent* event) {
                if(event->button() == Qt::LeftButton) {
                        m_cur_alpha = m_alpha_array[2];
                        m_state = 2;
                        emit clicked();
                }
        }

        void mouseReleaseEvent(QMouseEvent* event) {
                m_state = 1;
        }

public:
        image_button(QWidget* parent = 0,const QPixmap& pm = QPixmap())
                : QToolButton(parent), change_alpha(0.06), timer_ms(50) {
                m_state = 3;
                m_cur_alpha = 0.5;
                m_alpha_array[0] = 0.5;
                m_alpha_array[1] = 0.8;
                m_alpha_array[2] = 1.0;
                m_alpha_array[3] = 0.0;

                startTimer(timer_ms);
                set_pixmap(pm);
        }

        void set_normal_alpha(float a, bool change_cur = true) {
                m_alpha_array[0] = a;
                if(change_cur)
                        m_cur_alpha = a;
        }

        void set_hover_alpha(float a) { m_alpha_array[1] = a; }
        void set_press_alpha(float a) { m_alpha_array[2] = a; }
        void set_hide(bool hide) { if(hide) m_state = 3; else m_state = 0; }
        void set_pixmap(const QPixmap& pm) { 
                m_pixmap = pm;
                setFixedSize(pm.size());
        }
};

#endif   /* ----- #ifndef IMAGE_BUTTON_INC  ----- */

