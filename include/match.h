#ifndef  MATCH_INC
#define  MATCH_INC

#include "sinc/xml_easya.h"
#include <vector>

class match {
public:
        match() {
                home = "NBA";
                home_score = "";
                away_score = "";
                away = "NBA";
                state = "0";
                time = "";
        }

        match(xml_easya xe) {
                home = xe["home"].text();
                home_score = xe["home_score"].text();
                away_score = xe["away_score"].text();
                away = xe["away"].text();
                state = xe["state"].text();
                time = xe["time"].text();
        }

        const match& operator=(const match& m) {
                home = m.home;
                home_score = m.home_score;
                away_score = m.away_score;
                away = m.away;
                state = m.state;
                time = m.time;
                return *this;
        }

        std::string home;
        std::string home_score;
        std::string away_score;
        std::string away;
        std::string state;
        std::string time;
};

class match_list {
        std::vector<match> m_match_list;

public:
        match_list() { }
        void load_file(const std::string& filename) {
                TiXmlDocument doc(filename);
                doc.LoadFile();
                xml_easya total(doc);
                int num = total.how_many("match");
                m_match_list.clear();

                if(num == 0) {
                        m_match_list.push_back(match());
                        return;
                }

                for(int i = 0; i < num; i++) {
                        m_match_list.push_back(match(total["match"][i]));
                }
        }

        const match& index(int i) { return m_match_list[i]; }
        int size() { return int(m_match_list.size()); }
};

#endif   /* ----- #IFNDEF MATCH_INC  ----- */

