#ifndef  CONTROL_UI_INC
#define  CONTROL_UI_INC

#include "control_ui.uic"
#include <QtGui>

class control_ui : public QWidget, protected Ui_control {
        Q_OBJECT
protected:
        image_button* mb_close;
        void leaveEvent(QEvent* event);
        void enterEvent(QEvent* event);
        void resizeEvent(QResizeEvent* event);
        
public:
        control_ui(QWidget* parent = 0);

signals:
        void next_team();
        void click_close();
        void prev_team();
        void next_day();
        void prev_day();
};

#endif   /* ----- #ifndef CONTROL_UI_INC  ----- */
