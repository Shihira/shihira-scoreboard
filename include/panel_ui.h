#ifndef  panel_ui_INC
#define  panel_ui_INC

#include <tinyxml.h>
#include <QtGui>
#include "match.h"
#include "panel_ui.uic"

class panel_ui : public QWidget, protected Ui_panel {
        Q_OBJECT

protected:
        bool m_me_ready;
        bool m_dl_ready;

        QPropertyAnimation* movements[2];

public:
        typedef enum direction_t {
                left = 0,
                right = 1,
                up = 2,
                down = 3,
        } direction;

        panel_ui(QWidget* parent);
        // if in movement
        int state() { return movements[0] != 0 || movements[1] != 0; }

signals:
        void all_ready();
        void need_match(match& m);
        void finished();

private slots:
        void clean_up();
        void display(); // then finish
        void me_ready();

public slots:
        void dl_ready();
        void move(direction dir); // to start
        void change(match m);
};

#endif   /* ----- #ifndef panel_ui_INC  ----- */
